# ***********************************************************************************
# João Gabriel Camacho Presotto
# Trabalho 2 - Computação Inspirada pela Natureza
# Codificação do perceptron
# ***********************************************************************************

import random
import numpy as np
import math
from scipy.special import softmax
from decimal import Decimal

def ReLU(x):
	return max(0.0,x)

# Perceptron para reconhecer Dataset Iris e Wine (3 classes cada)
def perceptron(X_train,X_val,d_train,d_val,alfa,max_it):
	t, E = 1, 1
	N = X_train.shape[0]

	number_of_classes = d_train.shape[1]

	W = np.random.rand(number_of_classes,X_train.shape[1])
	b = np.random.rand(number_of_classes,N)
	#W = np.zeros((number_of_classes,X_train.shape[1]))
	#b = np.zeros((number_of_classes,N))
	
	y = np.zeros((number_of_classes,N))
	e = np.zeros((number_of_classes,N))

	E_train = np.array([])
	E_val = np.array([])

	while t < max_it and E > 0:
		#print("Época:", t)
		#print(W)

		E = 0
		for i in range(N):
			# Saidas da rede para Xi
			y[0,i] = np.dot(W[0],X_train[i]) + b[0,i]
			y[1,i] = np.dot(W[1],X_train[i]) + b[1,i]
			y[2,i] = np.dot(W[2],X_train[i]) + b[2,i]
			y[:,i] = softmax(y[:,i])

			# Determina o erro para cada umas das saídas y
			e[0,i] = d_train[i,0] - y[0,i]
			e[1,i] = d_train[i,1] - y[1,i]
			e[2,i] = d_train[i,2] - y[2,i]

			# Atualiza o vetor de pesos
			W[0] = W[0] + np.dot(alfa*e[0,i],X_train[i])
			W[1] = W[1] + np.dot(alfa*e[1,i],X_train[i])
			W[2] = W[2] + np.dot(alfa*e[2,i],X_train[i])

			# Atualiza o bias
			b[0,i] = b[0,i] + (alfa*e[0,i])
			b[1,i] = b[1,i] + (alfa*e[1,i])
			b[2,i] = b[2,i] + (alfa*e[2,i])

			# Acumula o erro (MSE)
			E = E + ((math.pow(e[0,i],2)  + math.pow(e[1,i],2) + math.pow(e[2,i],2))/3)

		# Se atingir um valor muito próximo de 0 reduz E para ficar próximo de 0
		if E < 0.1:
			if round(E,2) < 0.07:
				E = 0

		#print(E)

		E_train = np.append(E_train,E)

		# Valição ao fim de uma época
		aux = validation(X_val,d_val,W,b)
		E_val = np.append(E_val,aux)

		t += 1

	print("\nTreinamento concluído!\n")

	return W, b, E_train, E_val

# Função para validação ao fim de um epoch
def validation(X_val,d_val,W,b):
	N = X_val.shape[0]
	number_of_classes = d_val.shape[1]
	
	y = np.zeros((number_of_classes,N))
	e = np.zeros((number_of_classes,N))

	E_val = 0.0

	for i in range(N):
		# Saidas da rede para Xi
		y[0,i] = np.dot(W[0],X_val[i]) + b[0,i]
		y[1,i] = np.dot(W[1],X_val[i]) + b[1,i]
		y[2,i] = np.dot(W[2],X_val[i]) + b[2,i]
		y[:,i] = softmax(y[:,i])

		# Determina o erro para cada umas das saídas y
		e[0,i] = d_val[i,0] - y[0,i]
		e[1,i] = d_val[i,1] - y[1,i]
		e[2,i] = d_val[i,2] - y[2,i]

		# Acumula o erro (MSE)
		E_val = E_val + ((math.pow(e[0,i],2)  + math.pow(e[1,i],2) + math.pow(e[2,i],2))/3)

	# Se atingir um valor muito próximo de 0 reduz E para ficar próximo de 0
	if E_val < 0.1:
		if round(E_val,2) < 0.07:
			E_val = 0

	return E_val
