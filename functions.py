# ***********************************************************************************
# João Gabriel Camacho Presotto
# Trabalho 2 - Computação Inspirada pela Natureza
# Funções Gerais (Avaliação, geração de gráficos, etc)
# ***********************************************************************************

import numpy as np
from matplotlib import pyplot as plt
from sklearn.metrics import confusion_matrix
import os

# ***********************************************************************************
# Funções para escrever e carregar dados da memória
# ***********************************************************************************

# Lógica retirada de: https://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
def plot_confusion_matrix(y_true, y_pred, classes, type_of_data, file_path):
	title = "Matriz de confusão do " + str(type_of_data+".")

	cm = confusion_matrix(y_true, y_pred)

	#cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]

	fig, ax = plt.subplots()
	im = ax.imshow(cm, interpolation='nearest', cmap=plt.cm.Blues)
	ax.figure.colorbar(im, ax=ax)
	ax.set_title(title,weight='bold')
	# We want to show all ticks...
	ax.set(xticks=np.arange(cm.shape[1]),
		   yticks=np.arange(cm.shape[0]),
		   # ... and label them with the respective list entries
		   xticklabels=classes, yticklabels=classes,
		   ylabel='Classe real',
		   xlabel='Classe prevista')

	# Rotate the tick labels and set their alignment.
	plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
			 rotation_mode="anchor")

	# Loop over data dimensions and create text annotations.
	fmt = 'd'
	thresh = cm.max() / 2.
	for i in range(cm.shape[0]):
		for j in range(cm.shape[1]):
			ax.text(j, i, format(cm[i, j], fmt),
					ha="center", va="center",
					color="white" if cm[i, j] > thresh else "black")
	fig.tight_layout()

	fig.savefig(str(file_path),bbox_inches='tight')


def plot_errors(E_train,E_val,file_path):
	f1 = plt.figure()
	plt.plot(E_train,label='Treinamento')
	plt.plot(E_val,label='Validação')
	plt.title("Gráfico do erro médio quadrático.")
	plt.xlabel("Épocas")
	plt.ylabel("Erro")
	plt.legend()
	f1.savefig(str(file_path),bbox_inches='tight')

# Salva o vetor de pesos e bias no arquivo especificado em "file_path"
def save_model_to_file(W,b,dir_path,file_name):
	try:
		os.makedirs(dir_path)
	except FileExistsError:
	    # directory already exists
	    pass
	np.savez(str(dir_path)+str(file_name),W=W,b=b)

	print("Modelo salvo com sucesso!")

# Carrega da memória as matrizes W e b
def load_model_from_file(dir_path,file_name):
	W,b = [],[]

	if os.path.isfile(str(dir_path)+str(file_name)+".npz"):
		npzfile = np.load(str(dir_path)+str(file_name)+".npz")
		W = npzfile['W']
		b = npzfile['b']
	else:
		print("Arquivo com o modelo",str("\"" + file_name + ".npz\""),"não existe.")

	return W, b

# Salva dados de treinamento, validação e teste na memória
def save_data_to_file(X_train, X_val, X_test, d_train, d_val, d_test, dir_path, file_name):
	try:
		os.makedirs(dir_path)
	except FileExistsError:
	    # directory already exists
	    pass
	np.savez(str(dir_path)+str(file_name),X_train=X_train,X_val=X_val,X_test=X_test,d_train=d_train,d_val=d_val,d_test=d_test)

	print("Dados salvos com sucesso!")

# Carrega dados de treinamento, validação e teste da memória
def load_data_from_file(dir_path,file_name):
	X_train, X_val, X_test = [],[],[]
	d_train, d_val, d_test = [],[],[]

	if os.path.isfile(str(dir_path)+str(file_name)+".npz"):
		npzfile = np.load(str(dir_path)+str(file_name)+".npz")
		X_train, X_val, X_test = npzfile['X_train'],npzfile['X_val'],npzfile['X_test']
		d_train, d_val, d_test = npzfile['d_train'],npzfile['d_val'],npzfile['d_test']
	else:
		print("Arquivo com os dados\"",str("\"" + file_name + ".npz\""),"não existe.")

	return X_train, X_val, X_test, d_train, d_val, d_test