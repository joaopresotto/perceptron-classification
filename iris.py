# ***********************************************************************************
# João Gabriel Camacho Presotto
# Trabalho 2 - Computação Inspirada pela Natureza
# Funções específicas para o Dataset Iris
# ***********************************************************************************

import numpy as np
from scipy.stats import stats
from sklearn.model_selection import train_test_split
from scipy.special import softmax

# ***********************************************************************************
# Funções para gerar estatísticas
# ***********************************************************************************

# Função para calcular a taxa de acerto e erro do conjunto
# Retorna vetores com as classes previstas pelo modelo e com as classes reais para gerar a matriz de confusão
def evaluate_set(X,d,W,b,file_path,option):
	N = X.shape[0]
	acertos = 0

	predictions = np.zeros(N)
	real_classes = np.zeros(N)

	# Para cada uma das entradas de X_test
	for i in range(N):
		# Gera uma predição a partir de W e b
		predicted = predict(X,W,b,i)
		true_class = np.argmax(d[i])

		predictions[i] = predicted
		real_classes[i] = true_class

		# Se a classe prevista pelo modelo for a mesma da entrada, conta um acerto
		if true_class == predicted:
			acertos += 1

	print("\nQuantidade de entradas no conjunto de " + str(option) + ":",N)
	print("Quantidade de acertos:", acertos)
	print("Quantidade de erros:", N-acertos)
	print("Taxa de acerto: %.2f%%" % (acertos/N * 100))
	print("Taxa de erro: %.2f%%" % ((N-acertos)/N * 100), "\n")

	file = open(file_path, "w+")		
	file.write("Quantidade de entradas no conjunto de " + str(option) +": %d" % N)
	file.write("\nQuantidade de acertos: %d" % acertos)
	file.write("\nQuantidade de erros: %d" % (N-acertos))
	file.write("\nTaxa de acerto: %.2f%%" % (acertos/N * 100))
	file.write("\nTaxa de erro: %.2f%%" % ((N-acertos)/N * 100))
	file.close()

	return predictions, real_classes

# ***********************************************************************************
# Funções relacionadas ao Perceptron (validação, predição, etc)
# ***********************************************************************************

# Retorna a classe de um determinado item de X
# 0 -> I. Setosa
# 1 -> I. Versicolor
# 2 -> I. Virginica
def predict(X,W,b,i):
	y = np.zeros(3)

	# y = f(W1*x1 + W2*x2 + W3*x3 + W4*x4 + b)
	y[0] = W[0,0]*X[i,0] + W[0,1]*X[i,1] + W[0,2]*X[i,2] + W[0,3]*X[i,3] + b[0,i]
	y[1] = W[1,0]*X[i,0] + W[1,1]*X[i,1] + W[1,2]*X[i,2] + W[1,3]*X[i,3] + b[1,i]
	y[2] = W[2,0]*X[i,0] + W[2,1]*X[i,1] + W[2,2]*X[i,2] + W[2,3]*X[i,3] + b[2,i]

	# Softmax de cada Y[][i] para gerar Y normalizado, ex: [1,0,0]
	y = softmax(y)

	index_of_classification = np.argmax(y)

	#if index_of_classification == 0: # setosa [1, 0, 0] 
	#	result  = "I. setosa"
	#elif index_of_classification == 1: # versicolor [0, 1, 0]
	#	result = "I. versicolor"
	#else: # virginica [0, 0, 1]
	#	result = "I. virginica"	

	return index_of_classification

# Converte o vetor d (ex: [1 0 0]) para a string da classe correspondente
def convert_to_string(d):
	if np.array_equal(d,[1,0,0]): # setosa [1, 0, 0] 
		result  = "I. setosa"
	elif np.array_equal(d,[0,1,0]): # versicolor [0, 1, 0]
		result = "I. versicolor"
	else: # virginica [0, 0, 1]
		result = "I. virginica"	
	return result

# ***********************************************************************************
# Funções para escrever e carregar dados da memória
# ***********************************************************************************

# Função para analizar os dados do dataset e os dividir em conjunto de treino, teste e treinamento
def initialize_dataset(file_path,number_of_samples,number_of_attributes,number_of_classes,train_test_validate=[0.6, 0.2, 0.2],zscore=True):
	file = open(file_path,"r")

	X = np.zeros((number_of_samples,number_of_attributes))
	d = np.zeros((number_of_samples,number_of_classes))

	i, lines = 0, file.readlines()

	for line in lines:
		line = line.strip() # Removendo "/n"

		sepal_lenght, sepal_width, petal_lenght, petal_width, classification = line.split(",")

		X[i] = np.array([sepal_lenght, sepal_width, petal_lenght, petal_width])

		if classification == "Iris-setosa":
			d[i] = np.array([1,0,0])
		elif classification == "Iris-versicolor":
			d[i] = np.array([0,1,0])
		else: # classification == "Iris-virginica"
			d[i] = np.array([0,0,1])

		i+=1

	file.close()

	# Normalizando X por zscore (axis=0 -> colunas)
	if zscore:
		X = stats.zscore(X,axis=0)

	# Separando em treinamento, validação e teste
	X_train, X_val, d_train, d_val = train_test_split(X, d, test_size=train_test_validate[1]+train_test_validate[2], shuffle=True)
	X_val, X_test, d_val, d_test = train_test_split(X_val, d_val, test_size=0.5, shuffle=True)

	return X_train, X_val, X_test, d_train, d_val, d_test 
