# ***********************************************************************************
# João Gabriel Camacho Presotto
# Trabalho 2 - Computação Inspirada pela Natureza
# Programa principal
# ***********************************************************************************

import sys
import os
import iris
import wine
import functions
import perceptron as p

def main():
    os.system('clear')
    print("*******************************")
    print("\tTrabalho 2 CIN        ")
    print("*******************************")
    choice = input(""" 1: Iris dataset\n 2: Wine dataset\n 3: Sair\n*******************************\nEntre com a opção e pressione enter: """)

    if choice == "1":
        menu_iris()
    elif choice == "2":
        menu_wine()
    elif choice=="3":
        sys.exit
    else:
        print("\nOpção inválida, tente novamente.")
        input("Pressione enter para continuar...")
        main()

def menu_iris():    
    X_train, X_val, X_test = [],[],[]
    d_train, d_val, d_test = [],[],[]
    W,b = [], []

    os.system('clear')
    while True:
        print("*******************************")
        print("\t Iris Dataset        ")
        print("*******************************")
        choice = input(""" 1: Treinar Perceptron\n 2: Salvar resultado do treinamento no disco\n 3: Carregar dados de treinamento do disco\n 4: Avaliação experimental\n 5: Voltar\n*******************************\nEntre com a opção e pressione enter: """)

        if choice == "1":
            X_train, X_val, X_test, d_train, d_val, d_test = iris.initialize_dataset(file_path="Datasets/Iris/iris.data",number_of_samples=150,number_of_attributes=4,number_of_classes=3,zscore=False)
            W, b, E_train, E_val = p.perceptron(X_train,X_val,d_train,d_val,alfa=0.02,max_it=500)
            
            functions.plot_errors(E_train,E_val,file_path="Resultados Experimentais/Iris/erroIris.pdf")

        elif choice == "2":
            if W != []:
                print("")
                functions.save_model_to_file(W,b,dir_path="Modelos/",file_name="irisPerceptron")
                functions.save_data_to_file(X_train,X_val,X_test,d_train,d_val,d_test,dir_path="Modelos/",file_name="irisData")
                print("")
            else:
                print("Não há dados para serem salvos, execute o treinamento ou carregue da memória.")

        elif choice == "3":
            W_aux, b_aux = functions.load_model_from_file(dir_path="Modelos/",file_name="irisPerceptron")
            X_train_aux, X_val_aux, X_test_aux, d_train_aux, d_val_aux, d_test_aux = functions.load_data_from_file(dir_path="Modelos/",file_name="irisData")           
            
            if W_aux != []:
                W, b = W_aux, b_aux
                X_train, X_val, X_test, d_train, d_val, d_test = X_train_aux, X_val_aux, X_test_aux, d_train_aux, d_val_aux, d_test_aux
                print("\nDados carregados com sucesso!\n")

        elif choice == "4":
            if W != []:
                pred_test, true_test = iris.evaluate_set(X_test,d_test,W,b,file_path="Resultados Experimentais/Iris/avaliaçãoTeste.txt",option="teste")
                functions.plot_confusion_matrix(true_test,pred_test,classes=['I. setosa', 'I. versicolor', 'I. virginica'],type_of_data="conjunto de testes",file_path="Resultados Experimentais/Iris/matrizConfusaoTeste.pdf")

                pred_train, true_train = iris.evaluate_set(X_train,d_train,W,b,file_path="Resultados Experimentais/Iris/avaliaçãoTreino.txt",option="treino")
                functions.plot_confusion_matrix(true_train,pred_train,classes=['I. setosa', 'I. versicolor', 'I. virginica'],type_of_data="conjunto de treino",file_path="Resultados Experimentais/Iris/matrizConfusaoTreino.pdf")

                pred_val, true_val = iris.evaluate_set(X_val,d_val,W,b,file_path="Resultados Experimentais/Iris/avaliaçãoValidação.txt",option="validação")
                functions.plot_confusion_matrix(true_val,pred_val,classes=['I. setosa', 'I. versicolor', 'I. virginica'],type_of_data="conjunto de validação",file_path="Resultados Experimentais/Iris/matrizConfusaoValidacao.pdf")

            else:
                print("Não há um modelo para testes, execute o treinamento ou carregue da memória.")

        elif choice=="5":
            break;
        
        else:
            print("Opção inválida, tente novamente.")

        input("Pressione enter para continuar...")
        os.system('clear')

    main()

def menu_wine():
    X_train, X_val, X_test = [],[],[]
    d_train, d_val, d_test = [],[],[]
    W,b = [], []

    os.system('clear')
    while True:
        print("*******************************")
        print("\t Wine Dataset        ")
        print("*******************************")
        choice = input(""" 1: Treinar Perceptron\n 2: Salvar resultado do treinamento no disco\n 3: Carregar dados de treinamento do disco\n 4: Avaliação experimental\n 5: Voltar\n*******************************\nEntre com a opção e pressione enter: """)

        if choice == "1":
            X_train, X_val, X_test, d_train, d_val, d_test = wine.initialize_dataset(file_path="Datasets/Wine/wine.data",number_of_samples=178,number_of_attributes=13,number_of_classes=3,zscore=True)
            W, b, E_train, E_val = p.perceptron(X_train,X_val,d_train,d_val,alfa=0.0065,max_it=500)

            functions.plot_errors(E_train,E_val,file_path="Resultados Experimentais/Wine/erroWine.pdf")

        elif choice == "2":
            if W != []:
                print("")
                functions.save_model_to_file(W,b,dir_path="Modelos/",file_name="winePerceptron")
                functions.save_data_to_file(X_train,X_val,X_test,d_train,d_val,d_test,dir_path="Modelos/",file_name="wineData")
                print("")
            else:
                print("Não há dados para serem salvos, execute o treinamento ou carregue da memória.")

        elif choice == "3":
            W_aux, b_aux = functions.load_model_from_file(dir_path="Modelos/",file_name="winePerceptron")
            X_train_aux, X_val_aux, X_test_aux, d_train_aux, d_val_aux, d_test_aux = functions.load_data_from_file(dir_path="Modelos/",file_name="wineData")           
            
            if W_aux != []:
                W, b = W_aux, b_aux
                X_train, X_val, X_test, d_train, d_val, d_test = X_train_aux, X_val_aux, X_test_aux, d_train_aux, d_val_aux, d_test_aux
                print("\nDados carregados com sucesso!\n")

        elif choice == "4":
            if W != []:
                pred_test, true_test = wine.evaluate_set(X_test,d_test,W,b,file_path="Resultados Experimentais/Wine/avaliaçãoTeste.txt",option="teste")
                functions.plot_confusion_matrix(true_test,pred_test,classes=['Wine-1', 'Wine-2', 'Wine-3'],type_of_data="conjunto de testes",file_path="Resultados Experimentais/Wine/matrizConfusaoTeste.pdf")

                pred_train, true_train = wine.evaluate_set(X_train,d_train,W,b,file_path="Resultados Experimentais/Wine/avaliaçãoTreino.txt",option="treino")
                functions.plot_confusion_matrix(true_train,pred_train,classes=['Wine-1', 'Wine-2', 'Wine-3'],type_of_data="conjunto de treino",file_path="Resultados Experimentais/Wine/matrizConfusaoTreino.pdf")

                pred_val, true_val = wine.evaluate_set(X_val,d_val,W,b,file_path="Resultados Experimentais/Wine/avaliaçãoValidação.txt",option="validação")
                functions.plot_confusion_matrix(true_val,pred_val,classes=['Wine-1', 'Wine-2', 'Wine-3'],type_of_data="conjunto de validação",file_path="Resultados Experimentais/Wine/matrizConfusaoValidacao.pdf")
            else:
                print("Não há um modelo para testes, execute o treinamento ou carregue da memória.")

        elif choice=="5":
            break;
        
        else:
            print("Opção inválida, tente novamente.")

        input("Pressione enter para continuar...")
        os.system('clear')

    main()



if __name__ == '__main__':
    main()